SET check_function_bodies = false;
CREATE FUNCTION public.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$;
CREATE TABLE public.black_listed_user_names (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    user_name text NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now()
);
COMMENT ON TABLE public.black_listed_user_names IS 'Contains user names that are blacklisted by admin';
CREATE TABLE public.check_ins (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    title text,
    description text,
    avatar text,
    hlp_reward_points integer,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.check_ins IS 'Contains check-ins';
CREATE TABLE public.goal_check_ins (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    goal uuid NOT NULL,
    check_in uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.goal_check_ins IS 'Many to many relation between goals and check-ins';
CREATE TABLE public.goals (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    avatar text NOT NULL,
    age_group text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.goals IS 'Contains goals';
CREATE TABLE public.schedules (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "user" uuid NOT NULL,
    check_in uuid NOT NULL,
    tool_kit uuid NOT NULL,
    type text NOT NULL,
    schedule_type text NOT NULL,
    schedule_date date NOT NULL,
    "showReminder" boolean NOT NULL,
    "reminderTime" time with time zone NOT NULL,
    "repeatPerDay" integer NOT NULL
);
COMMENT ON TABLE public.schedules IS 'Contains check-in and tool kit schedule configured by the user';
CREATE TABLE public.tool_kit_category (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    age_group text NOT NULL,
    avatar text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.tool_kit_category IS 'Contains tool kit categories';
CREATE TABLE public.tool_kit_sub_category (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    tool_kit_category uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.tool_kit_sub_category IS 'Contains tool kit sub-categories';
CREATE TABLE public.tool_kits (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    title text NOT NULL,
    short_description text NOT NULL,
    description text NOT NULL,
    tool_kit_type text NOT NULL,
    tool_kit_category uuid NOT NULL,
    tool_kit_sub_category uuid NOT NULL,
    tool_kit_hlp_reward_points integer NOT NULL,
    tool_kit_type_value integer NOT NULL,
    video_thumb_nail text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.tool_kits IS 'Contains tool kits';
CREATE TABLE public.user_check_ins (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    check_in uuid NOT NULL,
    "user" uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.user_check_ins IS 'Contains check-ins that are selected by user ';
CREATE TABLE public.user_goals (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    goal uuid NOT NULL,
    "user" uuid NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.user_goals IS 'Contains goals that are selected by user';
CREATE TABLE public.user_tool_kits (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    "user" uuid DEFAULT gen_random_uuid() NOT NULL,
    tool_kit uuid DEFAULT gen_random_uuid() NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.user_tool_kits IS 'Contains tool kits added by user';
CREATE TABLE public.users (
    id uuid DEFAULT gen_random_uuid() NOT NULL,
    full_name text NOT NULL,
    user_name text NOT NULL,
    password text NOT NULL,
    salt text NOT NULL,
    puk_reference_id text,
    age_group text,
    accepted_terms_and_conditions boolean NOT NULL,
    avatar text,
    role text NOT NULL,
    last_login_time timestamp with time zone NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.users IS 'Contains user records';
ALTER TABLE ONLY public.black_listed_user_names
    ADD CONSTRAINT black_listed_user_names_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.check_ins
    ADD CONSTRAINT check_ins_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.goal_check_ins
    ADD CONSTRAINT goal_check_ins_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.goals
    ADD CONSTRAINT goals_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.schedules
    ADD CONSTRAINT schedules_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.tool_kit_category
    ADD CONSTRAINT tool_kit_category_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.tool_kit_sub_category
    ADD CONSTRAINT tool_kit_sub_category_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.tool_kits
    ADD CONSTRAINT tool_kits_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.user_check_ins
    ADD CONSTRAINT user_check_ins_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.user_goals
    ADD CONSTRAINT user_goals_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.user_tool_kits
    ADD CONSTRAINT user_tool_kits_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_id_puk_reference_id_key UNIQUE (id, puk_reference_id);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
CREATE TRIGGER set_public_black_listed_user_names_updated_at BEFORE UPDATE ON public.black_listed_user_names FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_black_listed_user_names_updated_at ON public.black_listed_user_names IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_check_ins_updated_at BEFORE UPDATE ON public.check_ins FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_check_ins_updated_at ON public.check_ins IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_goal_check_ins_updated_at BEFORE UPDATE ON public.goal_check_ins FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_goal_check_ins_updated_at ON public.goal_check_ins IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_goals_updated_at BEFORE UPDATE ON public.goals FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_goals_updated_at ON public.goals IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_tool_kit_category_updated_at BEFORE UPDATE ON public.tool_kit_category FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_tool_kit_category_updated_at ON public.tool_kit_category IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_tool_kit_sub_category_updated_at BEFORE UPDATE ON public.tool_kit_sub_category FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_tool_kit_sub_category_updated_at ON public.tool_kit_sub_category IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_tool_kits_updated_at BEFORE UPDATE ON public.tool_kits FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_tool_kits_updated_at ON public.tool_kits IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_user_check_ins_updated_at BEFORE UPDATE ON public.user_check_ins FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_user_check_ins_updated_at ON public.user_check_ins IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_user_goals_updated_at BEFORE UPDATE ON public.user_goals FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_user_goals_updated_at ON public.user_goals IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_user_tool_kits_updated_at BEFORE UPDATE ON public.user_tool_kits FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_user_tool_kits_updated_at ON public.user_tool_kits IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_users_updated_at BEFORE UPDATE ON public.users FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_users_updated_at ON public.users IS 'trigger to set value of column "updated_at" to current timestamp on row update';
ALTER TABLE ONLY public.goal_check_ins
    ADD CONSTRAINT goal_check_ins_check_in_fkey FOREIGN KEY (check_in) REFERENCES public.check_ins(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.goal_check_ins
    ADD CONSTRAINT goal_check_ins_goal_fkey FOREIGN KEY (goal) REFERENCES public.goals(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.schedules
    ADD CONSTRAINT schedules_check_in_fkey FOREIGN KEY (check_in) REFERENCES public.check_ins(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.schedules
    ADD CONSTRAINT schedules_tool_kit_fkey FOREIGN KEY (tool_kit) REFERENCES public.tool_kits(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.schedules
    ADD CONSTRAINT schedules_user_fkey FOREIGN KEY ("user") REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.tool_kit_sub_category
    ADD CONSTRAINT tool_kit_sub_category_tool_kit_category_fkey FOREIGN KEY (tool_kit_category) REFERENCES public.tool_kit_category(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.tool_kits
    ADD CONSTRAINT tool_kits_tool_kit_category_fkey FOREIGN KEY (tool_kit_category) REFERENCES public.tool_kit_category(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.tool_kits
    ADD CONSTRAINT tool_kits_tool_kit_sub_category_fkey FOREIGN KEY (tool_kit_sub_category) REFERENCES public.tool_kit_sub_category(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_check_ins
    ADD CONSTRAINT user_check_ins_check_in_fkey FOREIGN KEY (check_in) REFERENCES public.check_ins(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_check_ins
    ADD CONSTRAINT user_check_ins_user_fkey FOREIGN KEY ("user") REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_goals
    ADD CONSTRAINT user_goals_goal_fkey FOREIGN KEY (goal) REFERENCES public.goals(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_goals
    ADD CONSTRAINT user_goals_user_fkey FOREIGN KEY ("user") REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_tool_kits
    ADD CONSTRAINT user_tool_kits_tool_kit_fkey FOREIGN KEY (tool_kit) REFERENCES public.tool_kits(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_tool_kits
    ADD CONSTRAINT user_tool_kits_user_fkey FOREIGN KEY ("user") REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
