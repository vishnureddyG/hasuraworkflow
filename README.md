# hasuraworkflow

create .env file in root directory with following variables `HASURA_GRAPHQL_ENDPOINT`,
`HASURA_GRAPHQL_ADMIN_SECRET`
`HASURA_GRAPHQL_ACTIONS_HANDLER_WEBHOOK_BASEURL`.

1. Import metadat and run migration

```bash
hasura metadata apply

hasura migrations apply
```

2. Open console

```bash
hasura console
```

[Hasura cli env vars](https://hasura.io/docs/latest/graphql/core/hasura-cli/config-reference/)
